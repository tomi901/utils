﻿using UnityEngine;


namespace Interbrain.Utils
{
    public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        // TODO: Make these "overridable"
        protected const bool AutoCreateInstance = true;
        protected const bool DestroyOnLoad = true;
        protected const bool DestroyDuplicates = true;



        private static T instance;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    var foundInstances = FindObjectsOfType<T>();

                    if (foundInstances.Length > 0)
                    {
                        instance = foundInstances[0];

                        if (foundInstances.Length > 1)
                            MultipleInstancesFound();
                    }
                    else
                    {
                        instance = new GameObject(typeof(T).Name).AddComponent<T>();
                    }
                }

                return instance;
            }
        }


        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = (T)(object)this;
            }
            else if (instance != this)
            {
                MultipleInstancesFound();
                return;
            }

            /* TODO
            if (!DestroyOnLoad)
                DontDestroyOnLoad(gameObject);
            */
        }

        private static void MultipleInstancesFound()
        {
            Debug.LogWarning($"Multiple instances of {typeof(T)} found. This shouldn't happen.");
        }
    }
}
